<?php
   $acitve_menu = [
        'index' => "",
        'posts' => ""
   ];

//    echo "<pre>";
//    print_r($_SERVER);
//    print_r(pathinfo($_SERVER["REQUEST_URI"], PATHINFO_BASENAME));
//    echo "</pre>";
    if(pathinfo($_SERVER["REQUEST_URI"], PATHINFO_BASENAME)=="index.php"){
        $acitve_menu['index'] = "active-menu";
    }else if(pathinfo($_SERVER["REQUEST_URI"], PATHINFO_BASENAME)=="posts.php"){
        $acitve_menu['posts'] = "active-menu";
    }
?>


<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li>
                <div class="user-img-div">
                    <img src="assets/img/user.png" class="img-thumbnail" />

                    <div class="inner-text">
                        Jhon Deo Alex
                    <br />
                        <small>Last Login : 2 Weeks Ago </small>
                    </div>
                </div>
            </li>
            <li>
                <a class="<?=$acitve_menu['index']?>" href="index.php"><i class="fa fa-dashboard "></i>Dashboard</a>
            </li>
            <li>
                <a class="<?=$acitve_menu['posts']?>"  href="posts.php"><i class="fa fa-square-o "></i>Manage Posts</a>
            </li>
        </ul>
    </div>
</nav>