<?php
    session_start();
    include_once "../config/connection.php";
    include "user_session.php";
    include "functions/permission.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADMIN</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        include_once "blocks/header.php";
    ?>
    <main>
    <?php
        if(!isset($_SESSION['user_id'])){
            include_once "blocks/login.php";
        }else{
            include_once "blocks/nav.php";
            include_once "blocks/content.php";
        }
    ?>
    </main>
    <?php
        include_once "blocks/footer.php";
    ?>
</body>
</html>
<?php
    mysqli_close($conn);
?>