<?php
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecturer</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <h3><?=$_POST['name']." ".$_POST['lastname']?></h3>
        <form action="lectori.php" method="post">
            <table class="tb">
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Grade</th>
                    <th>Max Point</th>
                </tr>
                <?php
                    for($i=0; $i < count($_POST['question']); $i++){
                ?>
                <tr>
                    <td><?=$_POST['question'][$i]?></td>
                    <td><?=$_POST['answer'][$i]?></td>
                    <td><input type="number"></td>
                    <td><?=$_POST['point'][$i]?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                    </td>
                    <td>
                        <button>Graded</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>