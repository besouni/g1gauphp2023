<?php
    $url = "root";
    $url_a = "<a href=?dir=".$url.">".$url."</a>";

    if(isset($_GET["dir"])){
        if($_GET["dir"]!="root"){
            $url .= "/".$_GET["dir"];
            $url_a .= "/<a href=?dir=".$_GET["dir"].">".$_GET["dir"]."</a>";
        }
    }

    echo $url;

    if(isset($_POST["folder"]) and !empty($_POST["folder"])){
        mkdir($url."/".$_POST["folder"]);
    } 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Folders</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="form">
            <h2>Create Folder</h2>
            <form method="post">
                <input type="text" name="folder" placeholder="Folder Name">
                <br><br>
                <button>Create Folder</button>
            </form>
        </div>
        <div class="list">
            <h3> <?=$url_a?> Items</h3>
            <ul>
                <?php
                    // print_r(scandir("root"));
                    for($i=2; $i<count(scandir($url)); $i++){
                ?>
                    <li><a href="?dir=<?=scandir($url)[$i]?>"><?=scandir($url)[$i]?></a></li>
                <?php
                    }
                ?>
            </ul>
        </div>
    </div>
</body>
</html>