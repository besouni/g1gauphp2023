<?php
    include "upload.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 7</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="form">
            <h3>Form of Uploaded File</h3>
            <form method="post" enctype="multipart/form-data">
                <input type="file" name="f_name">
                <br><br>
                <button name="upload">Upload</button>
            </form>
        </div>
        <div class="list">
            <h3>Content Of Root Folder</h3>
            <table class="tb">
            <?php
                for($i=2; $i<count(scandir("root")); $i++){
            ?>
                <tr>
                    <td><?=scandir("root")[$i]?></td><td><a href='?file=<?="root/".scandir("root")[$i]?>'>DELETE</a></td>
                </tr>
            <?php
                }
            ?>
            </table>
        </div>
    </div>
</body>
</html>