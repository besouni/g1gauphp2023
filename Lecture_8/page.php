<?php
    include "file.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 8</title>
    <!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
        selector: '#mytextarea',
        plugins: [
          'a11ychecker','advlist','advcode','advtable','autolink','checklist','export',
          'lists','link','image','charmap','preview','anchor','searchreplace','visualblocks',
          'powerpaste','fullscreen','formatpainter','insertdatetime','media','table','help','wordcount'
        ],
        toolbar: 'undo redo | formatpainter casechange blocks | bold italic backcolor | ' +
          'alignleft aligncenter alignright alignjustify | ' +
          'bullist numlist checklist outdent indent | removeformat | a11ycheck code table help'
      });
    </script> -->
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main>
        <section class="left">
            <h3>Create File</h3>
            <form method="post">
                <input type="text" name="f_name" placeholder="Name of File">
                <button>Create File</button>
            </form>
            <h3>List of File</h3>
            <table class="tb_list">
                <?php
                    for($i=2; $i<count(scandir($dir)); $i++){
                ?>
                    <tr>
                        <td><?=scandir($dir)[$i]?></td>
                        <td><a href="?action=write&patch=<?=$dir."/".scandir($dir)[$i]?>">Write</a></td>
                        <td><a href="?action=read&patch=<?=$dir."/".scandir($dir)[$i]?>">Read</a></td>
                        <td><a href="?action=edit&patch=<?=$dir."/".scandir($dir)[$i]?>">Edit</a></td>
                    </tr>
                <?php } ?>
            </table>
        </section>
        <section class="right">
            <?php 
                if(isset($_GET["action"]) && $_GET["action"]=="write"){
                    include "write_form.php";
                }elseif(isset($_GET["action"]) && $_GET["action"]=="read"){
                    include "read_from_file.php";
                }elseif(isset($_GET["action"]) && $_GET["action"]=="edit"){
                    include "edit_file.php";
                }
            ?>
        </section>
    </main>
</body>
</html>